
*
* Rump BIOS for use with cpm400.bin produced from S-records
*

CR      EQU     $0D
LF      EQU     $0A

* -------------- S100Computers IDE BOARD PorT ASSIGNMENTS  (30-34H)

* Ports for 8255 chip. Change these to specify where the 8255 is addressed,
* and which of the ports on the 8255 are connected to which IDE signals.
* The first three control which 8255 ports have the IDE control signals,
* upper and lower data bytes.  The fourth one is for mode setting for the
* 8255 to configure its ports, which must correspond to the way that
* the first three lines define which ports are connected.

IDEportA        EQU     $FFFF0030       * Lower 8 bits of IDE interface
IDEportB        EQU     $FFFF0031       * Upper 8 bits of IDE interface
IDEportC        EQU     $FFFF0032       * Control lines for IDE interface
IDECtrlPort     EQU     $FFFF0033       * 8255 configuration port
IDEDrivePort    EQU     $FFFF0034       * To select the 1st or 2nd CF card/drive

READcfg8255     EQU     %10010010       * Set 8255 IDEportC out, IDEportA/B input
WRITEcfg8255    EQU     %10000000       * Set all three 8255 ports output

* IDE control lines for use with IDEportC.

IDEa0line       EQU     $01             * Direct from 8255 to IDE interface
IDEa1line       EQU     $02             * Direct from 8255 to IDE interface
IDEa2line       EQU     $04             * Direct from 8255 to IDE interface
IDEcs0line      EQU     $08             * Inverter between 8255 and IDE interface
IDEcs1line      EQU     $10             * Inverter between 8255 and IDE interface
IDEwrline       EQU     $20             * Inverter between 8255 and IDE interface
IDErdline       EQU     $40             * Inverter between 8255 and IDE interface
IDErstline      EQU     $80             * Inverter between 8255 and IDE interface

* Symbolic constants for the IDE Drive registers, this makes the
* code more readable than always specifying the address pins

REGdata         EQU     IDEcs0line
REGerr          EQU     IDEcs0line+IDEa0line
REGseccnt       EQU     IDEcs0line+IDEa1line
REGsector       EQU     IDEcs0line+IDEa1line+IDEa0line
REGcylLSB       EQU     IDEcs0line+IDEa2line
REGcylMSB       EQU     IDEcs0line+IDEa2line+IDEa0line
REGshd          EQU     IDEcs0line+IDEa2line+IDEa1line  *(0EH)
REGcommand      EQU     IDEcs0line+IDEa2line+IDEa1line+IDEa0line        *(0FH)
REGstatus       EQU     IDEcs0line+IDEa2line+IDEa1line+IDEa0line
REGcontrol      EQU     IDEcs1line+IDEa2line+IDEa1line
REGastatus      EQU     IDEcs1line+IDEa2line+IDEa1line+IDEa0line

* IDE Command Constants.  These should never change.

COMMandread     EQU     $20
COMMandwrite    EQU     $30
	
	
DISK_DRIVE	equ	$ff0004
DISK_FLUSH	equ	$ff0018
	
ACIASTAT	equ	$ffff0000
ACIADATA	equ	$ffff0001 
	

#ifdef SREC

_ccp:	equ	$4b8

	org 0
	dc.l	initstack
	dc.l	$400

*       BIOS start point 
        org $6000

#else
#ifdef LOADER
        global _bios
#else
        global _init
        global _ccp
#endif
        text

#endif


_init:	
#ifndef LOADER
        move.l	#traphndl,$8c	* setup trap #3 handler
	bsr 	charcold
#endif
	lea 	initmsg,a2
	bsr	prtstr
	move.l	#$2,d0		* default disk is drive C
	rts


traphndl:
#ifdef LOADER
_bios:
#endif
        cmpi	#nfuncs,d0
	bge	trapng
        lsl	#2,d0
	lea 	biosbase,a0
	move.l	(a0,d0),a0
	jsr	(a0)
trapng:	
#ifdef LOADER
        rts
#else
        rte
#endif


#ifndef LOADER
	
wboot:
	bsr	charwarm
	jmp	_ccp
#endif
getseg:	move.l	#memrgn,d0
	rts
	
setexc:
        andi.l  #$ff,d1         * do only for exceptions 0 - 255
        lsl     #2,d1           * multiply exception nmbr by 4
        movea.l d1,a0
        move.l  (a0),d0         * return old vector value
        move.l  d2,(a0)         * insert new vector
        rts

prtstr:                         * simple null terminated string printer
        move.b  (a2)+,d1
        tst.b   d1
        beq     print_done
        bsr     conout
        bra     prtstr
print_done:
        rts 

setiob:	move.w	d1,iobyte
	rts
getiob:	move.w	iobyte,d0
	rts

charcold:
        move.l  #$ffffffff,current_lba
charwarm:
  	rts

#ifndef LOADER
coninstat:
	btst.b	#1,ACIASTAT	* test receive buffer full flag
	bne	conrdy
	move.w	#0,d0
	rts
#endif
conrdy:
	move.w	#$ff,d0
	rts
conoutstat:
	btst.b	#2,ACIASTAT
	bne	conrdy
	move.w	#0,d0
	rts
#ifndef LOADER
conin:	btst.b	#1,ACIASTAT    
	beq	conin
	clr	d0
	move.b	ACIADATA,d0
        rts
#endif
putchar:	
conout:	btst.b	#2,ACIASTAT
	beq	conout
	move.b	d1,ACIADATA
	rts

flush:	move.l	d0,DISK_FLUSH
	clr.l	d0
	rts
	
auxinstat:
auxoutstat:	
lstout:
auxout:
auxin:	clr.l	d0
	rts

home:	clr.w	track
	rts

seldsk:	move.l  #$ffffffff,current_lba
#ifdef LOADER
        move.l  #2,d1           * Force C: drive (drive 2)
#else
        and.l	#$f,d1
#endif
	move.w	d1,drive	* save drive number for later
	asl.l	#2,d1		* index into dph table

	move.l	#dphtab,a0
	move.l	(a0,d1),d0
	rts

settrk:	move.w	d1,track
	rts
setsec: move.w	d1,sector
	rts

sectran:
	clr.l	d0
	movea.l	d2,a0
	ext.l	d1
	tst.l	d2
	beq	notran
	asl	#1,d1
	move.w	0(a0,d1),d0
	rts
notran:	move.l	d1,d0
	rts

setdma:
	move.l	d1,disk_dma	
	rts

read:   bsr     convert_to_lba	* On return D0 has LBA
        move.l  d0,d3           * Save a copy of LBA in D1 for later
        lsr.l   #2,d0           * Divide LBA by 4 to convert from 128 byte sectors to 512 byte sectors

        lea     current_lba,a2
        cmp.l   (a2),d0         * Is this LBA (in d0) the same as the current_lba we already have in memory?
        beq     L_05            * If so, we can skip readsector

        move.l  d0,(a2)
	bsr	readsector      * Read 512 bytes into diskbuf
        tst.l   d0
        beq     L_05
        rts                     * Return D0=1 in case of error

L_05:
*       Now we have a 512 byte sector, and we need to determine 
*       which of the four 128 sub blocks has been requested.
        and.l   #3,d3           * d3 <- LBA mod 4
        lsl.l   #7,d3           * d3 <- (LBA mod 4) * 128
        lea     diskbuf,a3
        add.l   d3,a3           * a3 <- diskbuf + offset

*       If *disk_dma, in A2, is even, then we can copy a longword at a time.
*       Otherwise, we have to copy a byte at a time.
        
        move.l  disk_dma,a2
        move.l  a2,d1
        and.l   #1,d1
        bne     read_byte_copy

        move.w  #32,d1          * copy 32 long words (128 bytes)
L_03:   move.l  (a3)+,(a2)+
*       dbra    d1,L_03
        subq.w  #1,d1
        bne     L_03
        bra     read_done

read_byte_copy:
        move.w  #128,d1          * copy 128 bytes        
L_04:   move.b  (a3)+,(a2)+
*       dbra    d1,L_04
        subq.w  #1,d1
        bne     L_04
read_done:
        clr.w   d0
	rts

#ifndef LOADER

write:	bsr	convert_to_lba	* D0 has LBA in terms of 128 byte sectors
        move.l  d0,d3           * Save a copy LBA in D1 for later
        lsr.l   #2,d0           * Div by 4 to convert to 512 byte sectors
        move.l  d0,d7           * Save LBA/4 for later use by writesector

        lea     current_lba,a2  
        cmp.l   (a2),d0         * Is this LBA (in d0) the same as the current_lba we already have in memory?
        beq     L_06            * If so, we can skip readsector

        move.l  d0,(a2)
        bsr     readsector      * read in the existing sector
        tst.l   d0
        beq     L_06
        rts                     * return D0 != 0 in case of error

L_06:
        and.l   #3,d3           * d3 <- LBA mod 4
        lsl.l   #7,d3           * d3 <- (LBA mod 4) * 128
        lea     diskbuf,a3         
        add.l   d3,a3           * a3 <- diskbuf + offset 

*       If *disk_dma, in A2, is even, then we can copy a longword at a time.
*       Otherwise, we have to copy a byte at a time.
        
        move.l  disk_dma,a2
        move.l  a2,d1
        and.l   #1,d1
        bne     write_byte_copy

        move.w  #32,d1          * copy 32 long words
L_01:   move.l  (a2)+,(a3)+
*       dbra    d1,L_01
        subq.w  #1,d1
        bne     L_01
        bra     write_done

write_byte_copy:
        move.w  #128,d1         * copy 128 bytes
L_02:   move.b  (a2)+,(a3)+
        subq.w  #1,d1
        bne     L_02

write_done:
        move.l  d7,d0           
	bsr	writesector     * write the 512 byte sector
        rts                     * Writesector will set D0=0 for ok, D0=1 for error
	
#endif

convert_to_lba:	clr.l	d0
	move.w	drive,d0	* get drive number
	move.l	d0,DISK_DRIVE
	asl.l	#2,d0		* index into dph table
	lea.l	dphtab,a0
	move.l	(a0,d0),d0	* get dph address
	beq	diskerr
	move.l	d0,a0
	move.l	14(a0),a0	* load dpb address
	move.w	(a0),d1		* and finally, sectors/track
	move.w	track,d0	 
	mulu	d1,d0
	clr.l	d1
	move.w	sector,d1
	add.l	d1,d0
	rts
diskerr:
        move.l  #1,d0
        rts


CRLF:   move.b  #CR,d1                  *Send CR/LF to CRT
        bsr     putchar
        move.b  #LF,d1
        bsr     putchar
        rts

putlong_d7:
        move.l  d7,d6                   *Print long in D7 on CRT, Note D6 destroyed
        swap    d6                      *Swap down upper word
        bsr     putword_d6
        move.l  d7,d6
        bsr     putword_d6
        rts

putword_d6:
        move.w  d6,d1                   *Note D1 is destroyed
        lsr.w   #8,d1                   *Shift upper byte to lower 8 bits
        lsr.w   #4,d1                   *Shift upper byte to lower 4 bits
        and.b   #$0F,d1                 *SAVE LOWER NIBble
        or.b    #$30,d1                 *CONVERT TO ASCII
        cmp.b   #$39,d1                 *SEE IF IT IS > 9
        ble.s   hexok2
        addq.b  #7,d1                   *ADD TO MAKE 10=>A

hexok2: bsr     putchar                 *Address lower high byte nibble
        move.w  d6,d1                   *Origional number again to D1
        lsr.w   #8,d1                   *Shift upper byte to lower 8 bits
        and.b   #$0F,d1                 *SAVE LOWER NIBble
        or.b    #$30,d1                 *CONVERT TO ASCII
        cmp.b   #$39,d1                 *SEE IF IT IS>9
        ble.s   hexok3
        addq.b  #7,d1                   *ADD TO MAKE 10=>A
hexok3: bsr     putchar

putbyte_d6:                             *Print HEX value in D6
        move.w  d6,d1                   *Origional number again to D1
        lsr.w   #4,d1                   *Shift upper byte to lower 4 bits
        and.b   #$0F,d1                 *SAVE LOWER NIBble
        or.b    #$30,d1                 *CONVERT TO ASCII
        cmp.b   #$39,d1                 *SEE IF IT IS>9
        ble.s   hexok4
        addq.b  #7,d1                   *ADD TO MAKE 10=>A

hexok4: bsr     putchar                 *Address lower high byte nibble
        move.w  d6,d1                   *Origional number again to D1
        and.b   #$0F,d1                 *SAVE LOWER NIBble
        or.b    #$30,d1                 *CONVERT TO ASCII
        cmp.b   #$39,d1                 *SEE IF IT IS>9
        ble.s   hexok5
        addq.b  #7,d1                   *ADD TO MAKE 10=>A

hexok5: bsr     putchar
        rts                             *All done
	
putbits_d6:                             *Display Byte bit pattern in D6
        move.l  d3,-(A7)                *Save D3
        move.l  D2,-(A7)                *Save D2
        move.b  #7,d3                   *Bit indicator (7,6,5...0)
        move.b  #8,d2                   *Bit count

putbit1:
        btst    d3,D6
        beq     show_0
        move.b  #'1',d1
        bsr     putchar
        bra     nextbit
show_0: move.b  #'0',d1
        bsr     putchar
nextbit:
        subq.b  #1,d3
        subq.b  #1,d2                   *8 bits total
        bne     putbit1
        move.L  (A7)+,d2                *Restore D2
        move.L  (A7)+,d3                *Restore D3
        rts
	
SHOWerrors:
        move.l  d1,-(a7)                *Save D1
        bsr     CRLF
        move.b  #REGstatus,d5           *Get status in status register
        bsr     iderd8d

        move.b  d1,d4
        and.b   #1,d4
        bne     MoreError               *Go to  REGerr register for more info. All OK if 01000000

        and.b   #$80,d1
        beq     not7
        lea     DRIVE_BUSY,a2           *Drive Busy (bit 7) stuck high.   Status =
        bra     doneerr

not7:   and.b   #$40,d1
        bne     not6
        lea     DRIVE_NOT_READY,a2      *Drive Not Ready (bit 6) stuck low.  Status =
        bra     doneerr

not6:   and.b   #$20,d1
        bne     not5
        lea     DRIVE_WR_FAULT,a2       *Drive write fault.    Status =
        bra     doneerr

not5:   lea     UNK_ERR,a2
        bra     doneerr

MoreError:                              *Get here if bit 0 of the status register indicted a problem
        move.b  #REGerr,d5              *Get error code in REGerr
        bsr     iderd8d
        move.b  d4,d6
        move.l  d1,-(a7)                *Save D1

        and.b   #$10,d6
        beq     note4
        lea     SEC_NOT_FOUND,a2
        bra     doneerr

note4:  and.b   #$80,d6
        beq     note7
        lea     BAD_BLOCK,a2
        bra     doneerr

note7:  and.b   #$40,d6
        beq     note6
        lea     UNRECOVER_ERR,a2
        bra     doneerr

note6:  and.b   #$4,d6
        beq     note2
        lea     INVALID_CMD,a2
        bra     doneerr

note2:  and.b   #$2,d6
        beq     note1
        lea     TRK0_ERR,a2
        bra     doneerr

note1:  lea     UNK_ERR1,a2
        bsr     prtstr
        bsr     CRLF
        move    (a7)+,d1
        or.b    d1,d1   *Set NZ flag
        rts

doneerr: 
        bsr     prtstr                  *Print message in A2, then display byte bit pattern in D6
        bsr     putbits_d6              *Show error bit pattern
        bsr     CRLF
        move    (a7)+,d1                *Get origional flags
        or.b    d1,d1                   *Set NZ flag
        rts

	
IDEwaitnotbusy:                         *Drive READY if 01000000
        move.w  #$FFFF,d6               *Timeout counter for drive to be ready
more_wait:
        move.b  #REGstatus,d5           *wait for RDY bit to be set
        bsr     iderd8d                 
        move.b  d4,d1
        and.b   #%11000000,d1
        Eor.b   #%01000000,d1
        beq     done_not_busy
        subq.w  #1,D6
        bne     more_wait
        move.l   #1,D0                  *Drive has not gone into ready state. Set D0=1 to indicate an error
        rts
done_not_busy:
        clr.l   D0                      *Clear D0 to indicate no error
        rts

* Wait for the drive to be ready to transfer data.
* Returns the drive status in D0
IDEwaitdrq:                             
        move.w  #$FFFF,D6               *Timeout counter for DRQ bit to be set.
more_drq:
        move.b  #REGstatus,d5           *wait for DRQ bit to be set
        bsr     iderd8d                 *Note AH or CH are unchanged
        move.b  d4,d1
        and.b   #%10001000,d1
        cmp.b   #%00001000,d1
        beq     done_drq
        subq.w  #1,d6
        bne     more_drq
        move.l  #1,D0                  *Drive has not gone into DRQ state. Set D0=1 to indicate an error
        rts
done_drq:
        clr.l   D0                      *Clear D0 indicate no error
        rts

	
readsector:                             *Note: Translate first in case of an error otherewise we
        bsr     wr_lba                  *will get stuck on bad sector
        bsr     IDEwaitnotbusy          *make sure drive is ready
        tst.l   d0
        beq     L_19
        bsr     SHOWerrors              *Returned with D0 !=0 if error
	move.l	#1,d0
	rts

L_19:   move.b  #COMMandread,d4
        move.b  #REGcommand,d5
        bsr     idewr8d                 *Send sec read command to drive.
        bsr     IDEwaitdrq              *wait until it has got the data
        tst.l   d0
        beq     L_20
        bsr     SHOWerrors
	move.l	#1,d0
	rts

L_20:   lea     diskbuf,a2	        *A2 <- diskbuf
        move.w  #256,d6                 *Read 512 bytes to D6

more_rd16:
        move.b  #REGdata,(IDEportC)     *REG regsiter address
        or.b    #IDErdline,(IDEportC)   *08H+40H, Pulse RD line

        move.b  (IDEportB),(a2)+        *Read the lower byte first
        move.b  (IDEportA),(a2)+        *Then the upper byte first

        move.b  #REGdata,(IDEportC)     *Deassert RD line
        subq.w  #1,d6
        bne     more_rd16

        move.b  #REGstatus,d5
        bsr     iderd8d
        and.b   #$1,d4
        beq     L_21
        bsr     SHOWerrors              *If error display status
	move.l	#1,d0
	rts
L_21:   clr.l	d0
	rts

* Do not need disk writing code for loader bios
#ifndef LOADER  


* Write a sector, specified by the 3 bytes in LBA,
* On error, set D0=1. D0=0 otherwise.
* Tell which sector we want to read from.
writesector:                            *Note: Translate first in case of an error otherewise we
        bsr     wr_lba                  *will get stuck on bad sector
        bsr     IDEwaitnotbusy          *make sure drive is ready
        tst.l   d0
        beq     L_22
        bsr     SHOWerrors
        move.l	#1,d0
	rts

L_22:   move.b  #COMMandwrite,d4
        move.b  #REGcommand,d5
        bsr     idewr8d                 *tell drive to write a sector
        bsr     IDEwaitdrq              *wait unit it wants the data
        tst.l   d0
        beq     L_23
        bsr     SHOWerrors
        move.l	#1,d0
	rts

L_23:   lea     diskbuf,a2
        move.w  #256,D6                   *256X2 bytes

        move.b  #WRITEcfg8255,(IDECtrlPort)

wrsec_ide:
        move.b  (a2)+,(IDEportB)
        move.b  (a2)+,(IDEportA)

        move.b  #REGdata,(IDEportC)
        or.b    #IDEwrline,(IDEportC)   *Send WR pulse
        move.b  #REGdata,(IDEportC)
        subq.w  #1,d6
        bne     wrsec_ide

        move.b  #READcfg8255,(IDECtrlPort)  *Set 8255 back to read mode

        move.b  #REGstatus,d5
        bsr     iderd8d
        and.b   #$1,d4
        beq     L_24
        bsr     SHOWerrors              *If error display status
        move.l  #1,d0
        rts
L_24:   clr.l   d0
        rts

#endif

wr_lba: 
* D0 has LBA value
        move.l  d0,d4                   * LBA mode, low sectors go directly
        move.b  #REGsector,d5           * Send info to drive
        bsr     idewr8d                 * Write to 8255 a register. Note: for drive we will have 0 - maxsec sectors only

	asr	#8,d4
        move.b  #REGcylLSB,d5
        bsr     idewr8d                 * Write to 8255 a register

	asr	#8,d4
        move.b  #REGcylMSB,d5           * Send high trk#
        bsr     idewr8d                 * Send high trk# (in dh) to ide drive. Note: high 8 bits ignored by ide drive

        move.b  #1,d4                   * For cpm, one sector at a time
        move.b  #REGseccnt,d5
        bsr     idewr8d                 * Write to 8255 a register
        rts

iderd8d:                                * READ 8 bits from IDE register @ D5, return info in D4
        move.b  d5,(IDEportC)           * Select IDE register, drive address onto control lines
        or.b    #IDErdline,(IDEportC)   * RD pulse pin (40H), Assert read pin
        move.b  (IDEportA),d4           * Return with data in D4
        move.b  d5,(IDEportC)           * Select IDE register, drive address onto control lines
        move.b  #0,(IDEportC)           * Zero all port C lines
        rts

idewr8d:                                        * WRITE Data in D4 to IDE register @ D5
        move.b  #WRITEcfg8255,(IDECtrlPort)     * Set 8255 to write mode
        move.b  d4,(IDEportA)                   * Get data put it in 8255 A port
        move.b  d5,(IDEportC)                   * Select IDE register, drive address onto control lines
        or.b    #IDEwrline,(IDEportC)           * Assert write pin
        move.b  d5,(IDEportC)                   * Select IDE register, drive address onto control lines
        move.b  #0,(IDEportC)                   * Zero all port C lines
        move.b  #READcfg8255,(IDECtrlPort)      * Config 8255 chip, read mode on return
        rts

lststat:
	rts

#ifndef SREC 
        data
#endif
initmsg:
#ifdef LOADER
        dc.b   13,10,'CP/M-68K LOADER BIOS Version 0.1 - S100 Flavor',13,10,0
#else
        dc.b   13,10,'CP/M-68K BIOS Version 0.1 - S100 Flavor',13,10,0
#endif
biosbase:
	dc.l	_init
#ifdef LOADER
        dc.l    trapng
        dc.l    trapng
        dc.l    trapng
#else
	dc.l	wboot
	dc.l	coninstat
	dc.l	conin
#endif
	dc.l	conout
	dc.l	lstout
	dc.l	auxout
	dc.l	auxin
	dc.l	home
	dc.l	seldsk
	dc.l    settrk
        dc.l    setsec
        dc.l    setdma
        dc.l    read 
#ifdef LOADER
        dc.l    trapng
#else         
        dc.l    write
#endif 
        dc.l    lststat
        dc.l    sectran
        dc.l    conoutstat
        dc.l    getseg    
        dc.l    getiob    
        dc.l    setiob    
        dc.l    flush     
        dc.l    setexc    
        dc.l    auxinstat 
        dc.l    auxoutstat

nfuncs	equ	(*-biosbase)/4


DRIVE_BUSY              dc.b   'Drive Busy (bit 7) stuck high.   Status = ',0
DRIVE_NOT_READY         dc.b   'Drive Ready (bit 6) stuck low.  Status = ',0
DRIVE_WR_FAULT          dc.b   'Drive write fault.    Status = ',0
UNK_ERR                 dc.b   'Unknown error in status register.   Status = ',0
UNK_ERR1                dc.b   'Unknown Error. Error Register = ',0
	
BAD_BLOCK               dc.b   'Bad Sector ID.    Error Register = ',0
UNRECOVER_ERR           dc.b   'Uncorrectable data error.  Error Register = ',0
READ_ID_ERROR           dc.b   'Error setting up to read Drive ID',CR,LF,0
SEC_NOT_FOUND           dc.b   'Sector not found. Error Register = ',0
INVALID_CMD             dc.b   'Invalid Command. Error Register = ',0
TRK0_ERR                dc.b   'Track Zero not found. Error Register = ',0
	
** This memory region table allows for testing of the CPM loader
** which puts the loaded system at $fe0000

memrgn: dc.w   1        * 1 memory region
        dc.l   $8000    * starts at 8000 hex
        dc.l   $d00000  * goes until d08000 hex * FIXME: figure out what this really should be

** table of dph structures
** zero indicates that the drive does not exist
dphtab:	dc.l	dph0
	dc.l	dph1
	dc.l	dph2
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0
	dc.l	0

** dph0 is for a 8 inch SS SD compatible file system in drive A
dph0:	dc.l	xlt0
	dc.w	0
	dc.w	0
	dc.w	0
	dc.l	dirbuf
	dc.l	dpb0
	dc.l	ckv0
	dc.l	alv0

dph1:	dc.l	xlt1
	dc.w	0
	dc.w	0
	dc.w	0
	dc.l	dirbuf
	dc.l	dpb1
	dc.l	ckv1
	dc.l	alv1

** dph2 is for a fixed disk of 16MB
dph2:	dc.l	0		* no translate
	dc.w	0
	dc.w	0
	dc.w	0
	dc.l	dirbuf
	dc.l	dpb2
	dc.l	0
	dc.l	alv2

** Note: Because asl automagically enforces alignment after a
** dc.b, do not split the values onto separate lines here. That
** really screws things up.
dpb0:	dc.w	26		* sectors/track
	dc.b	3,7		* bsh, blm
	dc.b	0,0		* null extent mask, fill
	dc.w	242		* disk size in blocks
	dc.w	63		* directory mask
	dc.w	0
	dc.w	16		* check vector size
	dc.w	2		* track offset
	
dpb1:	dc.w	40		* sectors/track
	dc.b	4,15		* bsh, blm
	dc.b	0,0		* null extent mask, fill
	dc.w	392		* disk size in blocks
	dc.w	191		* directory mask
	dc.w	0
	dc.w	48		* check vector size
	dc.w	2		* track offset
	

dpb2:	dc.w	256		* sectors/track
	dc.b	4,15		* bsh,blm  (2K blocks)
	dc.b	0,0		* extent mask, fill
	dc.w	8176		* disk size in blocks (minus 1)
	dc.w	4095		* directory mask
	dc.w	0
	dc.w	0		* check vector size (none)
	dc.w	1		* track offset

xlt0:	dc.w	0,6,12,18,24,4,10,16,22,2,8,14,20
	dc.w	1,7,13,19,25,5,11,17,23,3,9,15,21
	
xlt1:   dc.w   0,1,2,3,4,5,6,7
        dc.w   16,17,18,19,20,21,22,23
        dc.w   32,33,34,35,36,37,38,39
        dc.w   8,9,10,11,12,13,14,15
        dc.w   24,25,26,27,28,29,30,31
	
disk_dma:	dc.l	0
	
#ifndef SREC        
        bss
#endif

dirbuf:	ds.b	128

ckv0:	ds.b	18
ckv1:	ds.b	48
alv0:	ds.b	64
alv1:	ds.b	50
alv2:	ds.b	1024

track:	ds.w	1
sector:	ds.w	1
drive:	ds.w	1

iobyte:	ds.w	1

        even
current_lba: ds.l  1
diskbuf: ds.b    512

	ds.l	128
initstack:
	ds.l	1

* ~Font name~Courier New~
* ~Font size~10~
* ~Tab type~1~
* ~Tab size~4~
        end _init

all: 68030-Monitor.EVEN.bin 68030-Monitor.ODD.bin 68030-Monitor.bin 68030-Monitor.ram

68030-Monitor.1.mot: 68030-Monitor.X68
	./vasmm68k_mot -Fsrec -L 68030-Monitor.L68 68030-Monitor.X68 -o 68030-Monitor.1.mot
68030-Monitor.mot: 68030-Monitor.1.mot
	grep -v S3..000FE 68030-Monitor.1.mot > 68030-Monitor.mot
68030-Monitor.EVEN.bin: 68030-Monitor.mot
	srec_cat 68030-Monitor.mot -offset -0xE00000 -split 2 0 -fill 0x0 0x0 0x80000 -o 68030-Monitor.EVEN.bin -BINARY
68030-Monitor.ODD.bin: 68030-Monitor.mot
	srec_cat 68030-Monitor.mot -offset -0xE00000 -split 2 1 -fill 0x0 0x0 0x80000 -o 68030-Monitor.ODD.bin -BINARY
68030-Monitor.bin: 68030-Monitor.mot
#	srec_cat 68030-Monitor.mot -offset -0xFC0000 -fill 0x0 0x0 0x40000 -o 68030-Monitor.bin -BINARY
	srec_cat 68030-Monitor.mot -offset -0xE00000 -fill 0x0 0x0 0x40000 -o 68030-Monitor.bin -BINARY
68030-Monitor.ram: 68030-Monitor.mot
	srec_cat 68030-Monitor.mot -offset -0x20000 -o 68030-Monitor.ram -BINARY

clean:
	rm -rf *.bin *.L68 *.mot *.1.mot *.ram

burn_even: 68030-Monitor.EVEN.bin
	minipro  -p "SST39SF040 @PLCC32" -w $<

burn_odd: 68030-Monitor.ODD.bin
	minipro  -p "SST39SF040 @PLCC32" -w $<

burn: 68030-Monitor.bin
	minipro  -p "SST39SF040" -w $<

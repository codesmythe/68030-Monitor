PROGRAM ASCIIART;

VAR
    X: INTEGER;
    Y: INTEGER;
    I: INTEGER;
    CH: CHAR;
    DONE: BOOLEAN;
    CA: REAL;
    CB: REAL;
    A: REAL;
    B: REAL;
    T: REAL;

BEGIN
    FOR Y := -12 to 13 DO
      BEGIN
        FOR X := -39 to 38 DO
          BEGIN
            CA := X * 0.0458;
            CB := Y * 0.08333;
            A := CA;
            B := CB;
            I := 0;
            CH := ' ';
            DONE := FALSE;
            WHILE (NOT DONE) AND (I < 16) DO
              BEGIN
                T := A*A - B*B + CA;
                B := 2*A*B + CB;
                A := T;
                IF (A*A + B*B > 4.0) THEN 
                  BEGIN
                    IF I > 9 THEN I := I + 7;
                    CH := CHR(I+48);
		    DONE := TRUE
                  END;
                I := I + 1
              END;
            WRITE(CH)
          END;
       WRITELN("")
       END;
END.
